package com.example.task02;

public class DiscountBill extends Bill {
  private double discount;

  public void setDiscount(long discount) {
    if (discount < 0 || discount > 100) {
      throw new IllegalArgumentException("discount must be between 0 and 100");
    }
    this.discount = (double) discount / 100;
  }

  public long getDiscount() {
    return (long) (discount * 100);
  }

  @Override
  public long getPrice() {
    return (long) (super.getPrice() * (1 - discount));
  }

  public long getProfit() {
    return super.getPrice() - getPrice();
  }
}

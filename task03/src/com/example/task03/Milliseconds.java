package com.example.task03;

/**
 * Интервал в миллисекундах
 */
public class Milliseconds implements TimeUnit {
  private final long amount;

  public Milliseconds(long amount) {
    this.amount = amount;
  }

  @Override
  public long toMillis() {
    return amount;
  }

  @Override
  public long toSeconds() {
    return (amount + (amount < 0 ? -500 : 500)) / 1000;
  }

  @Override
  public long toMinutes() {
    return (amount + (amount < 0 ? -30000 : 30000)) / 60000;
  }

  @Override
  public long toHours() {
    return (amount + (amount < 0 ? -1800000 : 1800000)) / 3600000;
  }
}

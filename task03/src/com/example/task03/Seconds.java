package com.example.task03;

/**
 * Интервал в секундах
 */
public class Seconds implements TimeUnit {
  private final long amount;

  public Seconds(long amount) {
    this.amount = amount;
  }

  @Override
  public long toMillis() {
    return amount * 1000;
  }

  @Override
  public long toSeconds() {
    return amount;
  }

  @Override
  public long toMinutes() {
    return (amount + (amount < 0 ? -30 : 30)) / 60;
  }

  @Override
  public long toHours() {
    return (amount + (amount < 0 ? -1800 : 1800)) / 3600;
  }
}

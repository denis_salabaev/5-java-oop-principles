package com.example.task03;

import lombok.NonNull;

/**
 * Класс, в котором собраны методы для работы с {@link TimeUnit}
 */
public class TimeUnitUtils {
  /**
   * Конвертирует интервал в секундах в интервал в миллисекундах
   *
   * @param seconds интервал в секундах
   * @return интервал в миллисекундах
   */
  public static Milliseconds toMillis(@NonNull Seconds seconds) {
    return new Milliseconds(seconds.toMillis());
  }

  /**
   * Конвертирует интервал в миллисекундах в интервал в секундах
   *
   * @param millis интервал в миллисекундах
   * @return интервал в секундах
   */
  public static Seconds toSeconds(@NonNull Milliseconds millis) {
    return new Seconds(millis.toSeconds());
  }

  public static Milliseconds toMillis(@NonNull Minutes minutes) {
    return new Milliseconds(minutes.toMillis());
  }

  public static Milliseconds toMillis(@NonNull Hours hours) {
    return new Milliseconds(hours.toMillis());
  }

  public static Seconds toSeconds(@NonNull Minutes minutes) {
    return new Seconds(minutes.toSeconds());
  }

  public static Seconds toSeconds(@NonNull Hours hours) {
    return new Seconds(hours.toSeconds());
  }

  public static Minutes toMinutes(@NonNull Milliseconds millis) {
    return new Minutes(millis.toMinutes());
  }

  public static Minutes toMinutes(@NonNull Seconds seconds) {
    return new Minutes(seconds.toMinutes());
  }

  public static Minutes toMinutes(@NonNull Hours hours) {
    return new Minutes(hours.toMinutes());
  }

  public static Hours toHours(@NonNull Milliseconds millis) {
    return new Hours(millis.toHours());
  }

  public static Hours toHours(@NonNull Seconds seconds) {
    return new Hours(seconds.toHours());
  }

  public static Hours toHours(@NonNull Minutes minutes) {
    return new Hours(minutes.toHours());
  }
}

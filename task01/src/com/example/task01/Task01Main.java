package com.example.task01;

public class Task01Main {
  public static void main(String[] args) {
    Logger logger = Logger.getLogger("example");

    logger.setLevel(Logger.LoggerLevel.INFO);
    logger.log(Logger.LoggerLevel.DEBUG, "debug message");

    logger.info("info message");
    logger.warning("warning message");

    logger.error("Hello %s!", "World");
  }
}

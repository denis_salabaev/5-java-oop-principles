package com.example.task04;

public interface MessageHandler {
  void write(byte[] buf);
}

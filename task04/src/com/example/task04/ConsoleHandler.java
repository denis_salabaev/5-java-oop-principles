package com.example.task04;

import lombok.NonNull;

public class ConsoleHandler implements MessageHandler {
  public void write(@NonNull byte[] buf) {
    try {
      System.out.write(buf);
    } catch (Exception e) {}
  }
}

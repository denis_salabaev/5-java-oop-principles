package com.example.task04;

import java.io.FileOutputStream;
import java.io.IOException;

import lombok.NonNull;

public class FileHandler implements MessageHandler {
  private final FileOutputStream writer;

  public FileHandler(@NonNull String path) throws IOException {
    this.writer = new FileOutputStream(path);
  }

  public void write(@NonNull byte[] buf) {
    try {
      writer.write(buf);
    } catch (Exception e) {}
  }

  public void close() throws IOException {
    writer.close();
  }

  @Override
  protected void finalize() throws IOException {
    close();
  }
}

package com.example.task04;

import java.io.IOException;
import java.util.ArrayList;

import lombok.NonNull;

class MemoryHandler implements MessageHandler {
  private final MessageHandler handler;
  private final int bytesThreshold;

  private ArrayList<byte[]> buffers;
  private int lenBytes;

  public MemoryHandler(@NonNull MessageHandler handler, int bytesThreshold) {
    this.handler = handler;
    this.bytesThreshold = bytesThreshold;

    this.buffers = new ArrayList<>();
    this.lenBytes = 0;
  }

  public void write(byte[] buf) {
    buffers.add(buf);
    lenBytes += buf.length;

    if (lenBytes >= bytesThreshold) {
      flush();
    }
  }

  public void flush() {
    for (byte[] buf : buffers) {
      handler.write(buf);
    }

    buffers.clear();
    lenBytes = 0;
  }

  @Override
  protected void finalize() throws IOException {
    flush();
  }
}

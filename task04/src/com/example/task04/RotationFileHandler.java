package com.example.task04;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAmount;

import lombok.NonNull;

public class RotationFileHandler implements MessageHandler {
  private String basePath;
  private Instant rotationStart;
  private TemporalAmount interval;
  private FileOutputStream writer;

  private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm-ss");

  // basePath - путь до директории, в которой будут создаваться логи
  public RotationFileHandler(@NonNull String basePath, @NonNull TemporalAmount interval) throws IOException {
    this.basePath = basePath;
    this.interval = interval;

    rotate();
  }

  private void rotate() throws IOException {
    Instant now = Instant.now();
    Path logFilePath = Paths.get(basePath, formatter.format(now));

    rotationStart = now;

    try {
      FileOutputStream newWriter = new FileOutputStream(logFilePath.toString());
      if (writer != null) {
        writer.close();
      }
      writer = newWriter;
    } catch (IOException e) {
      if (writer == null) {
        throw e;
      }
    }
  }

  private boolean shouldRotate() {
    return rotationStart.plus(interval).compareTo(Instant.now()) > 0;
  }

  public void write(@NonNull byte[] buf) {
    try {
      if (shouldRotate()) {
        rotate();
      }
      writer.write(buf);
    } catch (Exception e) {}
  }
}

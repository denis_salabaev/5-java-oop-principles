package com.example.task04;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;

import lombok.NonNull;

public class Logger {
  public enum LoggerLevel {
    DEBUG(0, "DEBUG"),
    INFO(1, "INFO"),
    WARNING(2, "WARNING"),
    ERROR(3, "ERROR");

    private int priority;
    private String text;

    private LoggerLevel(int priority, String text) {
      this.priority = priority;
      this.text = text;
    }

    public int getPriority() {
      return priority;
    }

    public String toString() {
      return text;
    }
  }

  private static HashMap<String, Logger> loggers = new HashMap<>();
  private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy.MM.dd HH:mm:ss");

  private String name;
  private LoggerLevel level;
  private ArrayList<MessageHandler> handlers;

  private Logger(@NonNull String name) {
    this.name = name;
    this.level = LoggerLevel.DEBUG;
    this.handlers = new ArrayList<>();

    this.handlers.add(new ConsoleHandler());
  }

  public static Logger getLogger(@NonNull String name) {
    return loggers.computeIfAbsent(name, n -> new Logger(n));
  }

  public void clearHandlers() {
    this.handlers.clear();
  }

  public void setHandlers(@NonNull MessageHandler[] handlers) {
    if (!this.handlers.isEmpty()) {
      this.handlers.clear();
    }

    for (MessageHandler handler : handlers) {
      this.handlers.add(handler);
    }
  }

  public void addHandler(@NonNull MessageHandler handler) {
    this.handlers.add(handler);
  }

  public String getName() {
    return name;
  }

  public void setLevel(@NonNull LoggerLevel level) {
    this.level = level;
  }

  public LoggerLevel getLevel() {
    return level;
  }

  public void log(@NonNull LoggerLevel level, @NonNull String message) {
    if (level.getPriority() < this.level.getPriority()) {
      return;
    }

    LocalDateTime now = LocalDateTime.now();
    String dateTime = now.format(formatter);

    for (MessageHandler handler : handlers) {
      String formatted = String.format("[%s] %s %s - %s\n", level, dateTime, name, message);
      handler.write(formatted.getBytes());
    }
  }

  public void log(@NonNull LoggerLevel level, @NonNull String format, Object... args) {
    log(level, String.format(format, args));
  }

  public void debug(@NonNull String message) {
    log(LoggerLevel.DEBUG, message);
  }

  public void debug(@NonNull String format, Object... args) {
    log(LoggerLevel.DEBUG, format, args);
  }

  public void info(@NonNull String message) {
    log(LoggerLevel.INFO, message);
  }

  public void info(@NonNull String format, Object... args) {
    log(LoggerLevel.INFO, format, args);
  }

  public void warning(@NonNull String message) {
    log(LoggerLevel.WARNING, message);
  }

  public void warning(@NonNull String format, Object... args) {
    log(LoggerLevel.WARNING, format, args);
  }

  public void error(@NonNull String message) {
    log(LoggerLevel.ERROR, message);
  }

  public void error(@NonNull String format, Object... args) {
    log(LoggerLevel.ERROR, format, args);
  }
};
